select * from Shippers;

select Orders.ShipperID, count(orderID)
from Orders join Shippers on Orders.ShipperID=Shippers.ShipperID
group by Orders.ShipperID;

select CustomerName, OrderID
from Customers join Orders on Customers.CustomerID = Orders.CustomerID
order by OrderID;

select FirstName, LastName, OrderID
from Orders right join Employees on Orders.EmployeeID = Employees.EmployeeID
order by OrderID;